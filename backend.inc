<?php

abstract class CentralDataBackend {
  public $backend_id;
  public $name;
  public $backend;
  public $function;
  public $status;
  public $settings;

  public function __construct() {
    // @todo: constructor
  }

  public function status() {
    // @todo: return backend status
  }

  public function update() {
    // @todo: all update of backend
  }

  public function delete() {
    // @todo: remove backend
  }
}

interface CentralDataInterface {

  public function __construct();
  public function getStatus();
  public function loadContent($contentKey);
  public function pushContent($content);
  public function deleteContent($contentKey);
  public function queryContent($query);
  public function retrieveContentItem($contentKey);
  public function getEntityStructures();
  public function loadEntityStructure($entityStructureKey);
  public function pushEntityStructure($entityStructure);
  public function removeEntityStructure($entityStructureKey);

}
