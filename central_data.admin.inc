<?php

function central_data_configuration($form, &$form_state) {
  $form['central_data_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Central Data.'),
    '#default_value' => variable_get('central_data_enabled', 1),
    '#description' => t('When enabled content and structures will propagate as configured.'),
  );

  return system_settings_form($form);
}

function central_data_backend_edit_form($form, &$form_state) {
  $form['placeholder']['#markup'] = t("Placeholder edit form");
  return $form;
}

function central_data_backend_add_form($form, &$form_state) {
  $form['placeholder']['#markup'] = t("Placeholder add form");
  return $form;
}

function central_data_backend_delete($backend_id) {
  //@todo: remove backend
  return "Placeholder remove function " . $backend_id;
}

function central_data_structures($type) {
  //@todo: retrieve table from central_data_structures table
  //@todo: define central_data_structures table in schema
  return $type;
}

function central_data_content($type) {
  //@todo: retrieve central content
  return $type;
}

function central_data_backends($id = NULL) {
  $header = array(t("Data Backend"), t("Purpose"), t("Configuration Details"));
  $rows = array();

  $backend_ids = db_select('central_data_backends')
    ->fields('central_data_backends')
    ->orderBy('central_data_backends.name')
    ->orderBy('central_data_backends.id')
    ->execute()
    ->fetchCol();

  foreach ($backend_ids as $id) {
    $be = backend_load($id);
    $rows[] = array(
      "Example Backend",
      "Schema export",
      "Auto-export hourly",
      "EDIT",
    );
  }

  if (!empty($id)) {
    $rows[] = array(
      "Example Backend " . $id,
      "Schema export",
      "Auto-export hourly",
    );
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t("No classes configured."), 'colspan' => 3, 'class' => 'message'));
  }

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('style' => 'width: auto;'),
  ));

}
